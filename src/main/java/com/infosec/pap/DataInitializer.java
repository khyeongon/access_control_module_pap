package com.infosec.pap;

import lombok.extern.slf4j.Slf4j;

import org.casbin.jcasbin.main.Enforcer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

import com.infosec.pap.domain.User;
import com.infosec.pap.repository.UserRepository;

@Component
@Slf4j
public class DataInitializer implements CommandLineRunner {

    private static Logger log = LoggerFactory.getLogger(DataInitializer.class);

    public static Enforcer ENFORCER;
    
    @Autowired
    private Enforcer enforcer;

    @Autowired
    UserRepository users;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {

        if(enforcer != null) {
            log.info("enforcer is not null");
            ENFORCER = enforcer;
            enforcer.addPolicy("policy_admin", "pap", "read");
            enforcer.addPolicy("policy_admin", "pap", "write");
            enforcer.addGroupingPolicy("admin", "policy_admin");
        } else {
            log.info("enforcer is null");
        }
        this.users.save(User.builder()
            .username("user")
            .password(this.passwordEncoder.encode("password"))
            .roles(Arrays.asList("ROLE_USER"))
            .build()
        );

        this.users.save(User.builder()
            .username("user2")
            .password(this.passwordEncoder.encode("password"))
            .roles(Arrays.asList( "ROLE_USER"))
            .build()
        );

        this.users.save(User.builder()
            .username("admin")
            .password(this.passwordEncoder.encode("password"))
            .roles(Arrays.asList("ROLE_USER", "ROLE_ADMIN"))
            .build()
        );

        log.debug("printing all users...");
        this.users.findAll().forEach(v -> log.debug(" User :" + v.toString()));
    }
}