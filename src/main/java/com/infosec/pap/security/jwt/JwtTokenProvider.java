package com.infosec.pap.security.jwt;

import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {
    private Logger log = LoggerFactory.getLogger(JwtTokenAuthenticationFilter.class);
    @Autowired
    JwtProperties jwtProperties;

    @Autowired
    private UserDetailsService userDetailsService;

    private Key privateKey;
    private Key publicKey;

    @PostConstruct
    protected void init() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        String privateKeyContent = jwtProperties.getPrivateKey().replaceAll("\\n", "".replace("——BEGIN PRIVATE KEY——", "").replace("——END PRIVATE KEY——",""));
        String publicKeyContent = jwtProperties.getPublicKey().replaceAll("\\n", "".replace("——BEGIN PUBLIC KEY——", "").replace("——END PUBLIC KEY——",""));

        KeyFactory kf = KeyFactory.getInstance("RSA");
        
        /* Add PKCS#8 formatting if the key is in PKCS1*/
        // ASN1EncodableVector v = new ASN1EncodableVector();
        // v.add(new ASN1Integer(0));
        // ASN1EncodableVector v2 = new ASN1EncodableVector();
        // v2.add(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId()));
        // v2.add(DERNull.INSTANCE);
        // v.add(new DERSequence(v2));
        // v.add(new DEROctetString(Base64.getDecoder().decode(privateKeyContent)));
        // ASN1Sequence seq = new DERSequence(v);
        // byte[] privKey = seq.getEncoded("DER");
        //PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(privKey);


        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));

        privateKey = kf.generatePrivate(keySpecPKCS8);

        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
        publicKey = kf.generatePublic(keySpecX509);
    }

    public String createToken(String username, List<String> roles) {

        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", roles);

        Date now = new Date();
        Date validity = new Date(now.getTime() + jwtProperties.getValidityInMs());

        log.info("private key : " + privateKey.toString());
        
        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)//
            .signWith(privateKey, SignatureAlgorithm.RS256)
            .compact();
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody().getSubject();
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token);

            if (claims.getBody().getExpiration().before(new Date())) {
                return false;
            }

            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new InvalidJwtAuthenticationException("Expired or invalid JWT token");
        }
    }

}