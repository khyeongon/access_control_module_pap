package com.infosec.pap.security.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "jwt.key")
@Data
public class JwtProperties {

	private String privateKey; // = "it-is-a-secret-only-we-that-should-know";
    private String publicKey;
    
    //validity in milliseconds
    private long validityInMs = 3600000; // 1h    

    public JwtProperties() {
        
    }

    public String getPrivateKey() {
        return this.privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return this.publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public long getValidityInMs() {
        return this.validityInMs;
    }

    public void setValidityInMs(long validityInMs) {
        this.validityInMs = validityInMs;
    }
    

}